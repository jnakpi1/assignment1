package edu.towson.cosc435.jaiconakpil.assignment2

import kotlin.math.round
enum class BillTipCalculator {Tip10,Tip20,Tip30}

fun billCalculator (input: Double, calc: BillTipCalculator) : Double{

    var result = when (calc){
        BillTipCalculator.Tip10 -> (input * 0.1) + input
        BillTipCalculator.Tip20 -> (input * 0.2) + input
        BillTipCalculator.Tip30 -> (input * 0.3) + input

    }
    return result.round (2)
}

private fun Double.round (decimals: Int) : Double {
    var multiplier = 1.0
    repeat(decimals) {multiplier *=10}
    return round ( this*multiplier)/multiplier
}



