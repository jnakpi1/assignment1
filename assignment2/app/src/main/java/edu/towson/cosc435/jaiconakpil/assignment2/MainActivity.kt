package edu.towson.cosc435.jaiconakpil.assignment2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        calc_btn.setOnClickListener { handleClick() }
    }

    private fun handleClick(){
        try {

            val userInput = input_amt.editableText.toString()
            Toast.makeText(this, "Entered: $userInput", Toast.LENGTH_SHORT).show()

            val tipConverter = when (radioGroup.checkedRadioButtonId) {
                R.id.tenPercent_btn -> BillTipCalculator.Tip10
                R.id.twentyPercent_btn -> BillTipCalculator.Tip20
                R.id.thirtyPercent_btn -> BillTipCalculator.Tip30
                else -> throw Exception()
            }
            val inputValue = userInput.toDouble()
            val result = billCalculator(inputValue, tipConverter)
            result_tv.text=result.toString()
        }
        catch (e: Exception) {
            result_tv.text= "Calculation Error"

        }
    }
}
